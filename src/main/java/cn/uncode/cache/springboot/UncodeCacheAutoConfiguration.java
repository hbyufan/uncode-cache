package cn.uncode.cache.springboot;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.jedis.JedisClusterConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import cn.uncode.cache.CacheManager;
import cn.uncode.cache.framework.ICache;
import cn.uncode.cache.framework.aop.CacheAopService;
import cn.uncode.cache.framework.util.PropertiesUtil;
import cn.uncode.cache.store.CacheStore;
import cn.uncode.cache.store.local.CacheTemplate;
import cn.uncode.cache.store.redis.JedisTemplate;
import cn.uncode.cache.store.redis.cluster.JedisClusterCustom;
import cn.uncode.cache.store.redis.cluster.JedisClusterFactory;

/**
 * Created by Juny on 2017/2/28 14:11
 */
@Configuration
@ConditionalOnProperty(value = "uncode.cache.enabled", havingValue = "true")
public class UncodeCacheAutoConfiguration {
	
	private final Logger LOGGER = LoggerFactory.getLogger(UncodeCacheAutoConfiguration.class);
	
	@Autowired
	private Environment env;
	
	public static final String KEY_USE_LOCAL = "uncode.cache.useLocal";
	public static final String KEY_USE_STORE_REGION = "uncode.cache.storeRegion";
	
	
	private Properties buildProperties(){
		Properties properties = new Properties();
		int poolMaxIdle = JedisClusterFactory.POOL_MAX_IDLE;
		int poolMinIdle = JedisClusterFactory.POOL_MIN_IDLE;
		int poolMaxWaitMillis = JedisClusterFactory.POOL_MAX_WAIT_MILLIS;
		int poolMaxTotal = JedisClusterFactory.POOL_MAX_TOTAL;
		int timeOut = JedisClusterFactory.TIME_OUT;
		int maxRedirections = JedisClusterFactory.MAX_REDIRECTIONS;
		if(null != env){
			String address = env.getProperty(JedisClusterFactory.KEY_REDIS_CLUSTER_ADDRESS, String.class);
			if(StringUtils.isNotBlank(address)){
				properties.put(JedisClusterFactory.KEY_REDIS_CLUSTER_ADDRESS, 
						env.getProperty(JedisClusterFactory.KEY_REDIS_CLUSTER_ADDRESS, String.class));
			}else{
				LOGGER.error("redis address不能为空");
			}
			Integer maxIdle = env.getProperty(JedisClusterFactory.KEY_REDIS_POOL_MAX_IDLE, Integer.class);
			if(null != maxIdle){
				poolMaxIdle = maxIdle;
			}
			properties.put(JedisClusterFactory.KEY_REDIS_POOL_MAX_IDLE, poolMaxIdle);
			
			Integer minIdle = env.getProperty(JedisClusterFactory.KEY_REDIS_POOL_MIN_IDLE, Integer.class);
			if(null != minIdle){
				poolMinIdle = minIdle;
			}
			properties.put(JedisClusterFactory.KEY_REDIS_POOL_MIN_IDLE, poolMinIdle);
			
			
			Integer maxWaitMillis = env.getProperty(JedisClusterFactory.KEY_REDIS_POOL_MAX_WAIT_MILLIS, Integer.class);
			if(null != maxWaitMillis){
				poolMaxWaitMillis = maxWaitMillis;
			}
			properties.put(JedisClusterFactory.KEY_REDIS_POOL_MAX_WAIT_MILLIS, poolMaxWaitMillis);
            properties.put(JedisClusterFactory.KEY_REDIS_CLUSTER_PASSWORD, env.getProperty(JedisClusterFactory.KEY_REDIS_CLUSTER_PASSWORD, String.class));

            Integer maxTotal = env.getProperty(JedisClusterFactory.KEY_REDIS_POOL_MAX_TOTAL, Integer.class);
            if(null != maxTotal){
				poolMaxTotal = maxTotal;
			}
			properties.put(JedisClusterFactory.KEY_REDIS_POOL_MAX_TOTAL, poolMaxTotal);
			
			Integer timeout = env.getProperty(JedisClusterFactory.KEY_REDIS_CLUSTER_TIMEOUT, Integer.class);
			if(null != timeout){
				timeOut = timeout;
			}
			properties.put(JedisClusterFactory.KEY_REDIS_CLUSTER_TIMEOUT, timeOut);
			
			Integer maxRedistions = env.getProperty(JedisClusterFactory.KEY_REDIS_CLUSTER_MAX_REDIRECTIONS, Integer.class);
			if(null != maxRedistions){
				maxRedirections = maxRedistions;
			}
			properties.put(JedisClusterFactory.KEY_REDIS_CLUSTER_MAX_REDIRECTIONS, maxRedirections);
			
			String storeRegion = env.getProperty(KEY_USE_STORE_REGION, String.class);
			if(StringUtils.isNotBlank(storeRegion)){
				properties.put(KEY_USE_STORE_REGION, storeRegion);
			}else{
				LOGGER.error("store region不能为空");
			}
			Boolean useLocal = env.getProperty(KEY_USE_LOCAL, Boolean.class);
			if(null != useLocal){
				properties.put(KEY_USE_LOCAL, useLocal);
			}else{
				properties.put(KEY_USE_LOCAL, false);
			}
		}
		return properties;
	}
	
	@Bean(name = "jedisCluster")
	public JedisClusterCustom jedisClusterCustom(){
		JedisClusterFactory jedisClusterFactory = new JedisClusterFactory();
		PropertiesUtil.loadPorperties(buildProperties());
		try {
			jedisClusterFactory.init();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		JedisClusterCustom jedisClusterCustom = null;
		try {
			jedisClusterCustom = (JedisClusterCustom)jedisClusterFactory.getObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		LOGGER.info("===Uncode-Cache===UncodeCacheAutoConfiguration===>JedisClusterCustom inited..");
		return jedisClusterCustom;
	}
	
	@Bean
	public JedisConnectionFactory jedisConnectionFactory() {
		Map<String, Object> map = new HashMap<>();
		if(null != env){
			String address = env.getProperty(JedisClusterFactory.KEY_REDIS_CLUSTER_ADDRESS, String.class);
			if(StringUtils.isNotBlank(address)){
				address = address.replaceAll(";", ",");
				map.put("spring.redis.cluster.nodes", address);
			}
		}
		MapPropertySource mapPropertySource = new MapPropertySource("redis", map);
		RedisClusterConfiguration clusterConfiguration = new RedisClusterConfiguration(mapPropertySource);
		String password = env.getProperty(JedisClusterFactory.KEY_REDIS_CLUSTER_PASSWORD, String.class);
		if(StringUtils.isNotBlank(password)){
			clusterConfiguration.setPassword(RedisPassword.of(password));
		}
		JedisConnectionFactory connectionFactory = new JedisConnectionFactory(clusterConfiguration);
		return connectionFactory;
	}
	
	@Bean(name = "uncodeCache")
	public ICache<Object, Object> cache(JedisClusterCustom jedisClusterCustom){
		boolean useLocal = PropertiesUtil.getProperty4Boolean(KEY_USE_LOCAL, false);
    	JedisTemplate jedisTemplate = new JedisTemplate(jedisClusterCustom);
    	CacheTemplate cacheTemplate = new CacheTemplate(jedisTemplate, useLocal, true);
    	String sr = PropertiesUtil.getProperty(KEY_USE_STORE_REGION);
    	if(StringUtils.isBlank(sr)){
    		sr = "default_store_region";
    	}
    	ICache<Object, Object> cache = new CacheStore(sr, cacheTemplate);
		LOGGER.info("===Uncode-Cache===UncodeCacheAutoConfiguration===>Uncode cache inited..");
		return cache;
	}
	
	
	@Bean(name = "configCacheManager")
	public CacheManager<ApplicationEvent> cacheManager(ICache<Object, Object> cache){
		CacheManager<ApplicationEvent> configCacheManager = new CacheManager<ApplicationEvent>();
		configCacheManager.setStoreRegion(PropertiesUtil.getProperty(KEY_USE_STORE_REGION));
		LOGGER.info("===Uncode-Cache===UncodeCacheAutoConfiguration===>CacheManager inited..");
		return configCacheManager;
	}

    @Bean
    public CacheAopService cacheAopService(){
        LOGGER.info("===Uncode-Cache===UncodeCacheAutoConfiguration===>CacheAopService inited..");
        return new CacheAopService();
    }
	
}
