package cn.uncode.cache.framework.util;

import java.io.UnsupportedEncodingException;

public class ByteUtil {

	public static byte[] stringToByte(String str) {
		if(str==null){
			return null;
		}
		try {
			return str.getBytes("ISO-8859-1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String byteToString(byte[] bytes) {
		if(bytes==null){
			return null;
		}
		try {
			return new String(bytes,"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
