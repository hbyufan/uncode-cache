package cn.uncode.cache.store.redis.lock;


/**
 * 基于redis的分布式锁
 * @author juny.ye
 *
 */
public interface DistributeLock {
	
	
	/**
	 * 加锁操作
	 * @return 
	 */
	public boolean lock();
	public boolean lock(int seconds);

	/**
	 * 释放锁操作
	 * @return
	 */
    public boolean unlock();

}
