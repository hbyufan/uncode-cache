package cn.uncode.cache.subscribe;

public interface EventSubscribe {
	
	String subscribeTopic();

	void onMessage(String message);
}
